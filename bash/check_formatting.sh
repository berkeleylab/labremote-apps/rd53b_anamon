#!/bin/bash

RESET="\033[0m"
RED="\033[1m\033[31m"
BLUE="\033[1m\033[34m"
YELLOW="\033[1m\033[33m"
GREEN="\033[1m\033[32m"

function print_usage {

    printf " Usage: source bash/check_format.sh [OPTIONS]\n"
    printf "\n"
    printf " Options:\n"
    printf "     -h|--help      show this help message\n"
}

function check_files {

    apply_format=${1}
    if [[ "${apply_format}" == "1" ]]; then
        printf "${YELLOW} -- check_format: Applying formatting fixes to files that fail formatting check!${RESET}\n"
    fi

    format_status="0"
    extensions=".cpp .h"
    for extension in ${extensions};
    do
        printf "${BLUE} -- check_format: Checking *${extension} files %s${RESET}\n"
        files=$(find ./src/ -name *${extension})
        for fname in ${files};
        do
            printf "${BLUE} -- check_format: > file:  ${fname}${RESET}\n"
            $(cpplint --quiet ${fname}  > /dev/null)
            if [[ "$?" == "1" ]]; then
                printf "${RED} -- check_format: FORMAT FAILURE: File ${fname} fails formatting check:${RESET}\n"
                ./ci/run-clang-format.py ${fname}
                if [[ "${apply_format}" == "1" ]]; then
                    printf "${YELLOW} -- check_format: Applying fixes to ${fname}...${RESET}\n"
                    clang-format -i ${fname}
                fi
                format_status="1"
            fi
        done
    done
    if [ ! ${format_status} -eq "0" ];
    then
        return 1
    else
        return 0
    fi
}

function main {

    apply_format=0
    while test $# -gt 0
    do
        case $1 in
            -h)
                print_usage
                return 0
                ;;
            --help)
                print_usage
                return 0
                ;;
            --apply)
                apply_format=1
                ;;
            *)
                printf "${RED} check_format: Invalid argument provided: $1\n ${RESET}"
                return 1
        esac
        shift
    done
                
    format_ok=0
    if ! check_files ${apply_format}; then
        format_ok=1
    fi
    if [[ "${format_ok}" == "1" ]]; then
        printf "${RED} -- check_format: RESULT: FORMATTING FAILED${RESET}\n"
        return 1
    else
        printf "${GREEN} -- check_format: RESULT: FORMATTING SUCCESS${RESET}\n"
        return 0
    fi
}

#______________
main $*

