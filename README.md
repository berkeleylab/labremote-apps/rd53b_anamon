# RD53b Analog Monitor

<div align="center">
    <figure>
        <img src="docs/images/analog_monitor_card.png" height = "400">
        <figcaption>Image of the LBNL RD53B SCC Analog Monitor Card, with Adafruit FT232H breakout board mounted.</figcaption>
    </figure>
</div>

The goal of this package is to provide a simple interface to the LBNL RD53B Single-chip card (SCC) Analog
Monitor card. Users should include this package as a dependency to their own
packages and link to the built library in order to use the analog monitor card
for their own needs.

Additional information on the analog monitor card can be found on the [RD53B Testing Twiki](https://twiki.cern.ch/twiki/bin/viewauth/RD53/RD53BTesting#AnalogMonitor_Card_for_SCC_LBNL).

If you are just getting started with an SCCAnalogMonitor card, you should follow the route:
1. [Install the `rd53b_anamon` software](#installation)
2. [Define the configuration file for your SCCAnalogMonitor card](#sccanalogmonitor-configuration)
3. [Give your SCCAnalogMonitor card a unique identity](#setting-the-identity-of-the-sccanalogmonitor-card)
4. [Start monitoring](#run-a-monitoring-sequence)

Table of Contents
===
<!-------------------------- TABLE OF CONTENTS -------------------------------->
<!-------------------------- TABLE OF CONTENTS -------------------------------->
<!-------------------------- TABLE OF CONTENTS -------------------------------->
   * [Requirements](#requirements)
     * [FT232 Communication](#ft232-communication)
   * [Installation](#installation)
   * [SCCAnalogMonitor Configuration](#sccanalogmonitor-configuration)
     * [Connection Configuration](#sccanalogmonitor-connection-configuration)
     * [NTC Configuration](#sccanalogmonitor-ntc-configuration)
     * [ADC Configuration](#sccanalogmonitor-adc-configuration)
   * [Give the SCCAnalogMonitor Card a Unique Identity](#setting-the-identity-of-the-sccanalogmonitor-card)
   * [Run a Monitoring Sequence](#run-a-monitoring-sequence)
     * [Output configuration](#output-configuration)
     * [Datastream configuration](#datastreams-configuration)
     * [Measurement configuration](#measurement-configuration)
     * [Additional Notes](#additional-notes)
   * [Other `ana-mon` Utilities](#other-ana-mon-utilities)
      * [list](#list)
      * [meas](#meas)
   * [Using libRD53BAnalogMonitor in Your Own Package](#using-the-library)
   * [Determining the Steinhart-Hart Coefficients for the NTC](#determining-the-steinhart-hart-coefficients-for-the-ntc)
   * [Gotchas](#gotchas)
 
## Requirements

### FT232 Communication

Communication via USB between the PC the FT232H on-board the Adafruit shield requires that the PC
can communicate with the MPSSE engine on-board the FT232H. This requires:
   * [libmpsse](https://www.ftdichip.com/Support/SoftwareExamples/MPSSE.htm): Library that implements common communication protocols (I2C, SPI) using the MPSSE framework on FTDI chips
      * This [repository](https://github.com/devttys0/libmpsse]) has proven a reliable source
   * [libftdi](https://www.intra2net.com/en/developer/libftdi/index.php): Generic FTDI library

These requirements are needed for implementation of [I2CFTDICom](https://gitlab.cern.ch/berkeleylab/labRemote/-/blob/devel/src/libDevCom/I2CFTDICom.h) of labRemote.

## Installation

Once you have installed `libmpsse` and `libftdi` on your system you are ready to compile
`rd53b_anamon` as follows:

```bash
$ git clone --recursive ssh://git@gitlab.cern.ch:7999/berkeleylab/labremote-apps/rd53b_anamon.git
$ cd rd53b_anamon/
$ source /opt/rh/devtoolset-7/enable # gcc 7
$ mkdir build/
$ cd build/
$ cmake3 ..
$ make -j4
```

This will checkout and install `rd53b_anamon` (along with `labRemote`) and compile.

Upon successful compilation, in addition to the [libRD3BAnalogMonitor library](#using-the-library), you will have an executable `ana-mon` located under
under `build/bin`. The `ana-mon` executable is the main entry-point for [running basic
monitoring sequences](#run-a-monitoring-sequence).

## SCCAnalogMonitor Configuration

In order to successfully communicate with and use the LBL SCCAnalogMonitor card, you
are required to define an SCCAnalogMonitor configuration JSON file. A default
one is provided in the repository: [config/scc_anamon_config.json](config/scc_anamon_config.json).

There are several fields in the SCCAnalogMonitor configuration that one needs to specify
in order to have proper measurements. These are detailed below in the description
of the SCCAnalogMonitor configuration JSON file.

The SCCAnalogMonitor configuration JSON file has the following format:
```json
{
    "rd53b_scc_analog_monitor":
    {
        "connection": {...},
        "ntc": {...},
        "adc": {...}
    }
}
```

### SCCAnalogMonitor `connection` configuration
The `connection` field in the SCCAnalogMonitor configuration allows a user to provide
 detailed information for how to find and communicate with the SCCAnalogMonitor card
 and is as follows:
 ```json
 "connection":
 {
     "serial_number": "c0cac01a"
 }
 ```
 A description of the `connection` fields is as follows:
 * `serial_number`: This is the FT232H USB device `SERIAL NUMBER` attribute, provide a 32-bit hex string. This field allows for multiple FT232H devices (including SCCAnalogMonitor cards) since you can give your SCCAnalogMonitor card a unique `serial_number` value to differentiate it.
 
### SCCAnalogMonitor `ntc` configuration
The `ntc` field in the SCCAnalogMonitor configuration allows a user to describe
the NTC part used for performing the temperature measurements and is as follows:
```json
"ntc":
{
    "ntc_part": "103KT1608",
    "r_ref": 10000,
    "steinhart_coeffs": [1, 2, 3]
}
```
A description of the `ntc` fields is as follows:
* `ntc_part`: String that specifies the actual part number of the NTC
* `r_ref`: Reference resistance used in voltage divider on SCC that includes the NTC
* `steinhart_coeffs`: This is an array containing the [Steinhart-Hart Equation](https://en.wikipedia.org/wiki/Steinhart%E2%80%93Hart_equation) coefficients for the NTC being used, in the order of `[A, B, C]`. See the [following section on determining these coefficients for your NTC](#determining-the-steinhart-hart-coefficients-for-the-ntc). These cofficients are required in order for `rd53b_anamon` to compute the temperature measurements.

### SCCAnalogMonitor `adc` configuration
The `adc` field in the SCCAnalogMonitor configuration allows a user to provide
detailed information on the ADC on the SCCAnalogMonitor card that is used to
sample the monitoring pins on the SCC. It is defined as follows:
```json
"adc":
{
    "vref": 2.5
}
```
A description of the `adc` fields is as follows:
* `adc`: Number defining the voltage reference on the SCCAnalogMonitor card's ADC, in Volts


## Setting the Identity of the SCCAnalogMonitor Card

It is recommended that you provide your SCCAnalogMonitor card with a set of unique
identifier strings so that in the cases in which you either have multiple SCCAnalogMonitor
cards connected to the PC, or that you have other FT232H devices in addition to
your SCCAnalogMonitor card connected to the PC, you are able to still connect to it.

The FT232H chip on the SCCAnalogMonitor device that the PC communicates with has
a factory burned-in USB identity of `0x0403:0x6014`. All FT232H devices have this
USB identity, so if you have multiple devices with an FT232H on them, it is no longer
possible to uniquely identify them by these USB identity strings alone. 

For these reasons, there is the [set_scc_id](bash/set_scc_id.sh.in) utility that gets
built and placed in the build directory's `bin/` directory. This utility allows
the user to specify the FT232H `SERIAL NUMBER` attribute as follows:
```bash
$ ./bin/set_scc_id -s c0cac01a
```
After doing so, your SCCAnalogMonitor card will appear to the host PC with several
identifier strings in addition to the `0x0403:0x6014`:
```bash
$ ./bin/ftdi_list
[INFO]    : Found 1 FTDI devices matching 0000:0000
[INFO]    : Device #0 (0403:6014)
[INFO]    :     MANUFRACTURER ID : dantrim
[INFO]    :     PRODUCT ID       : SCCAnalogMonitor
[INFO]    :     SERIAL NUMBER    : c0cac01a
```
By default, the `set_scc_id` utility sets the device's `MANUFACTURER ID` string to
`${USER}`, but this can be changed by providing a value to the `-n` (or `--name`)
command line option of `set_scc_id`:
```bash
$ ./bin/set_scc_id -s c0cac01a -n foobar
...
$ ./bin/ftdi_list
[INFO]    : Found 1 FTDI devices matching 0000:0000
[INFO]    : Device #0 (0403:6014)
[INFO]    :     MANUFRACTURER ID : foobar
[INFO]    :     PRODUCT ID       : SCCAnalogMonitor
[INFO]    :     SERIAL NUMBER    : c0cac01a
```
The device's `PRODUCT ID` is not user configurable and will always be set to `SCCAnalogMonitor`
by `set_scc_id`.

The `SERIAL NUMBER` field that gets set in the hardware of the SCCAnalogMonitor
corresponds to the `connection : serial_number` field of the SCCAnalogMonitor configuration
described in the [SCCAnalogMonitor Configuration](#sccanalogmonitor-configuration) section.
If you have multiple SCCAnalogMonitor cards (or, generally, multiple FT232H devices),
giving your SCCAnalogMonitor card a unique `SERIAL NUMBER` allows it to be found
by the host PC unambiguously.

**Note 1**: You will be surprised at how many USB devices are sporting an FT232H in them, so it is recommended
to always set a unique address for your SCCAnalogMonitor card using `set_scc_id`.

**Note 2**: When using `set_scc_id`, you can only have a *single* FT232H device connected to your PC, otherwise
it is ambiguous as to which device should be the one to have it's identification strings updated.

## Run a Monitoring Sequence

In order to run a monitoring sequence, you must provide the `ana-mon` a monitoring
configuration JSON file in addition to the [SCCAnalogMonitor configuration file
described above](#sccanalogmonitor-configuration). For example, such a monitoring sequence
would be initiated as follows:
```bash
$ ./bin/ana-mon -c ../config/scc_anamon_config.json -r ../config/mon_config.json
```

The `-r` (`--run`) command line argument takes as input a JSON configuration file
which defines the monitoring setup. A default monitoring configuration
is provided under [config/mon_config_default.json](config/mon_config_default.json).

The structure of the SCCAnalogMonitor monitoring configuration is as follows:
```json
{
    "monitor_config" :
    {
        "output" : {...},
        "datastreams" : {...},
        "datasinks" : {...},
        "measurements" : {...}
    }
}
```
More detailed description of each of these fields are provided in the subsequent sections.

### Output configuration
The `output` fields specify how and to where any output data and/or files are stored. Right now it is:

```json
"output" :
{
    "directory" : "/path/to/monitor/data/",
    "suffix" : ""
}
```
- The field `directory` will be the directory where any data files are stored.
- If the `directory` field is an empty string (`""`) then the output directory will be defined as the default one (`/path/to/rd53b_anamon/data`) defined by the [create_default_data_dir](https://gitlab.cern.ch/berkeleylab/labremote-apps/rd53b_anamon/-/blob/add_mon/src/FileUtils.cpp#L102) function. 
- The field `suffix` is a string that will be be added to the end of any output files. If it is an empty string (`""`) or not present than no suffix will be added.

### Datastreams configuration

The `datastreams` object specifies which of the defined `datasinks` should be used
during the monitoring run to report the measurements. For example, the following configures the monitoring run
to stream the measurement data to two datasinks which are named `CSV` and `Console`:
```json
"datastreams" :
{
    "rd53b_anamon":
    {
        "sinks": ["CSV", "Console"]
    }
}
```

The named datasinks appearing within the `"datastreams"` configuration must be defined
in the configuration node named `datasinks`. The concept of a `datasink` is
defined in `labRemote` and their configuration is defined in `labRemote`. The
name of a datasink to be used by the `"datastreams"` field defines a JSON object under
the `"datasinks"` field, with a datasink's configuration provided within the associated
JSON object. For example, `labRemote` supports three main types of datasinks:
1. `CSVSink`: dumps measurement data to a CSV file
2. `ConsoleSink`: dumps the measurement data in a tabulated format to `stdout`
3. `InfluxDBSink`: dumps the measurement data to an `influxDB` instance so that the measurement data may be monitored graphically in an associated Grafana instance

As such, the `rd53b_anamon` configuration for the datasinks would appear as follows:
```json
"datasinks":
{
    "CSV": {"sinktype": "CSVSink", ...}, # further parameters omitted
    "Console": {"sinktype": "ConsoleSink", ...}, # further parameters omitted
    "influxDB": {"sinktype": "InfluxDBSink", ...} # further parameters omitted
}
```
You can see that the above configuration for `"datastreams"` specifies that the measurement
data will be reported using the datasinks named `CSV` and `Console`, whose configuration
is specified under the `"datasinks"` object.

Only those datasinks whose names appear under the `"datastreams" : "sinks"` list will
be used during a monitoring run, so you can define datasinks under `"datasinks"` that
are not necessarily used at runtime.

### Measurement configuration

The `measurement` object describes how measurements are taken and what the measurements are of:

```json
"measurement" :
{
    "tag" : "RD53B-AnalogMonitor-SCC"
    ,"name" : "rd53b_monitoring"
    ,"frequency" : 1000
    ,"count" : -1
    ,"time" : -1
    ,"quantities" : []
    ,"powersupply":
    {
        "devices" : {...}
        ,"channels" : ["Analog", "Digita"]
    }
}
```

- The `tag` field is the `DataSink` tag (`sink->setTag`) and gets called once per measurement.
- The `name` field is the name of the measurement (provided to `sink->startMeasurement(<name>,...)`). The `name` field also defines what the output `CSV` files will be named when using `CSVSink` (e.g. `<name>.csv`).
- The `frequency` field specifies the measurement frequency **in units of milliseconds** and must be a positive integer.
- The `count` field specifies the number of measurements to take. If the `count` field is negative or zero (`< 0`) then the measurements will ignore the counting (this allows for an infinite number of measurements, for example).
- The `time` field specifies for how long to continue taking measurements **in units of milliseconds**. If the `time` field is negative or zero (`< 0`) then the measurements will not keep track of time.
- If both the `count` and `time` field are positive, the `count` field will be given priority.
- The `quantities` array specifies which quantities to measure on the analog monitor card. If this array is empty (`[]`) then all of the SCC monitoring pins will be sampled. An example of specifying specific quantities is: `"quantities" : ["VDDA", "VDDD", "TP_NTC1"]`.
- If running infinitely (the `time` and `count` fields are both  negative), or if the specified `time` or number of `count` measurements have not been fulfilled, the monitoring can be killed with `Ctrl-C` or `Ctrl-Z`: these interrupts will be caught and the `DataSink` objects will exit gracefully.
- The `powersupply` field specifies the SCC powersupply:
   - The `devices` field should match the same schema as used in [labRemote's `libPS`](https://gitlab.cern.ch/berkeleylab/labRemote/-/tree/master/src/libPS).
   - The `channels` field is an array of strings, with names for the powersupply channels that you wish to monitor. The powersupply channel associated with the name is specified by the index in the array.
   - For each powersupply channel monitored, the voltage setting, voltage measurement, and current measurement will be monitored.
   - Providing the `--no-ps` command-line option to `ana-mon` will prevent monitoring of the powersupply channels.

### Additional notes

#### Specifying the environmental conditions
The `ana-mon` utility also has a command-line option `-t` (`--temp`) for specifying the external temperature of the RD53B. This is useful if you are running in a climate controlled setting. This option takes as argument the specified temperature **in units of Celsius**. If provided, an additional field in the recorded measurements will be provided for specifying this set temperature. This extra field is called `EnvTemp`. 

In order to ensure that the data formats of the output data sinks are somewhat consistent, the temperature field `EnvTemp` is always present, whether or not the user provided the `-t` (`--temp`) input. If the `t` (`--temp`) input is **not** provided, the `EnvTemp` data will be reported as `NaN`.

## Other `ana-mon` utilities

Here we describe several other runtime specifications for `ana-mon`. They
can be seen by providing the `-h` or `--help` command-line option:



```bash
$ ./bin/ana-mon -h
---------------------------------------------------------------------------------
 ana-mon

 Usage: ana-mon [options]

 Options:
  -c|--config          : Provide JSON configuration for SCC analog monitor card
  -r|--run             : Provide a monitoring config and start monitoring
  -t|--temp            : Provide a temperature (if climate controlled) in degrees Celsius
  --no-ps              : Do not monitor powersupply channels
  -l|--list            : List the quantities that can be measured and exit
  -m|--meas [STRING]   : Perform a measurement of a specified quantity and exit
  -d|--debug           : Increase log verbosity
  -h|--help            : Print this help message and exit
---------------------------------------------------------------------------------
```

### list

Running `ana-mon` with the `--list` option prints to screen all of the quantities
that are sampled by the ADCs on-board the analog monitor card:

```bash
$ ./bin/ana-mon --list
[INFO]    : -----------------------------
[INFO]    : [ 0]       VDDD (ADC 0, ch 0)
[INFO]    : [ 1]      VREFA (ADC 0, ch 1)
[INFO]    : [ 2]       VDDA (ADC 0, ch 2)
[INFO]    : [ 3]      REXTA (ADC 0, ch 3)
[INFO]    : [ 4]       VIND (ADC 0, ch 4)
[INFO]    : [ 5]      REXTD (ADC 0, ch 5)
[INFO]    : [ 6]       VINA (ADC 0, ch 6)
[INFO]    : [ 7]      VREFD (ADC 0, ch 7)
[INFO]    : [ 8]   VREF_ADC (ADC 1, ch 0)
[INFO]    : [ 9]   VMUX_OUT (ADC 1, ch 1)
[INFO]    : [10]    VOFS_LP (ADC 1, ch 2)
[INFO]    : [11]     R_IREF (ADC 1, ch 3)
[INFO]    : [12]       VOFS (ADC 1, ch 4)
[INFO]    : [13]    VDD_PRE (ADC 1, ch 5)
[INFO]    : [14]    TP_NTC1 (ADC 1, ch 6)
[INFO]    : [15]   VREF_OVP (ADC 1, ch 7)
[INFO]    : -----------------------------
```

As a user, you do not need to know the specifics of which specific ADC is responsible
for measuring a specific quantity, or what ADC channel that quantity corresponds to.
The user really only needs to provide the strings that appear above and
the functions that are responsible for sampling the ADCs will perform the ADC and
channel resolution.

### meas

If you want to quickly perform a single-shot measurement of a quantity that
is sampled by the ADCs, you can use the `--meas` command:

```bash
$ ./bin/ana-mon --meas VDDD
[INFO]    : Measurement: VDDD = 3 counts (0.001221 V)
```

## Determining the Steinhart-Hart Coefficients for the NTC

Under the [config/ntc_data](config/ntc_data) directory there are several text files
that contain datasheet data for several NTCs that allow one to determine the
Steinhart-Hart coefficients. In fact, in these text files, the Steinhart-Hart coefficients
are listed at the top: `A=...,B=...,C=...`.

You can use the script [python/fit_ntc.py](python/fit_ntc.py) to load in NTC data formatted
as those text files in [config/ntc_data](config/ntc_data) and perform a fit
to the Steinhart-Hart equation in order to extract the three coefficients `A`,`B`, and `C` that are
required for the `steinhart_coeffs` field of the SCCAnalogMonitor `ntc` configuration field.

## Using the Library

Upon successful compilation, a shared object  `libRD53BAnalogMonitor` will
be available. Any package requiring an interface
to the RD53B SCC analog monitor card should link against this library.

Once your package is linked to `libRD53BAnalogMonitor` and you provide the
necessary include directory in your Makefile conifguration, you can do:

```c++
#include "RD53BAnalogMonitor.h"
...
int main(...)
{
    std::string scc_config_filename = ...; // get from somewhere
    RD53BAnalogMonitor anamon;
    if(anamon.init(scc_config_filename))
    {
        std::cout << "Communication with RD53B Analog Monitor card successful!"
    }
    else
    {
        return 1;
    }
    
    double vddd_measurement = anamon.read("VDDD"); // measurement in volts
    int32_t vddd_counts = anamon.readCount("VDDD"); // measurement in ADC counts (12-bit)
    
    std::vector<std::string> quantities = { "VDDA", "VDDD", "TP_NTC1", "VMUX_OUT" };
    auto measured_quantities = anamon.read(quantities); // read a list of quantities all at once
    for(auto q : quantities)
    {
        std::cout << q << " = " << measured_quantities.at(q) << " Volts" << std::endl;
    }
    return 0;
}
```

See [ana-mon --list](#list) for more information on the quantities that can be measured.

## Gotchas

1. Ensure your `udev` rules are appropriate to allow for FTDI to access the USB drivers

### Enabling FTDI driver access
The device permissions need to be loosened to allow usage of the libftdi driver as a non-root user. The following udev rule allow any user in the `plugdev` group to access the FTDI232H device. 

```
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6014", MODE:="0666", GROUP:="plugdev"
```

Add it to a file called `/etc/udev/rules.d/83-ftdi.rules` and run the following command to load it:
```shell
udevadm control --reload-rules && udevadm trigger
```
2. In case you encounter
```
In file included from /home/user/labremote-apps/rd53b_anamon/src/RD53BAnalogMonitor.cpp:12:0:
/usr/local/include/mpsse.h:9:10: fatal error: ftdi.h: No such file or directory
 #include <ftdi.h>
          ^~~~~~~~
compilation terminated.
```
If ``ftdi.h`` is located in ``/usr/include/libftdi1/ftdi.h`` then a symblink is needed
```
ln -s /usr/include/libftdi1/ftdi.h /usr/include/ftdi.h
```
