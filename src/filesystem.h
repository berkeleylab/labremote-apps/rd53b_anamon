// We haven't checked which filesystem to include yet
#ifndef INCLUDE_STD_FILESYSTEM_EXPERIMENTAL

#if __cplusplus > 201703L

// Check for feature test macro for <filesystem>
#if defined(__cpp_lib_filesystem)
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#define INCLUDE_STD_FILESYSTEM 1
#define USE_FILESYSTEM 1

// Check for feature test macro for <experimental/filesystem>
#elif defined(__cpp_lib_experimental_filesystem)
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1
#define INCLUDE_STD_FILESYSTEM 0
#define USE_FILESYSTEM 1

// We can't check if headers exist...
// Let's assume experimental to be safe
#elif !defined(__has_include)
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#define INCLUDE_STD_FILESYSTEM 0
#define USE_FILESYSTEM 0

// Check if the header "<filesystem>" exists
#elif __has_include(<filesystem>)

#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#define INCLUDE_STD_FILESYSTEM 1
#define USE_FILESYSTEM 1

// Check if the header "<filesystem>" exists
#elif __has_include(<experimental/filesystem>)
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1
#define INCLUDE_STD_FILESYSTEM 0
#define USE_FILESYSTEM 1

// Fail if neither header is available with a nice error message
#else
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#define INCLUDE_STD_FILESYSTEM 0
#define USE_FILESYSTEM 0
//#       error Could not find system header "<filesystem>" or
//"<experimental/filesystem>"
#endif

#if INCLUDE_STD_FILESYSTEM_EXPERIMENTAL
#pragma message("Found std::experimental::filesystem")
#elif INCLUDE_STD_FILESYSTEM
#pragma message("Found std::filesystem")
#endif

//// We priously determined that we need the exprimental version
//#   if INCLUDE_STD_FILESYSTEM_EXPERIMENTAL
//// Include it
//#       include <experimental/filesystem>
//
//// We need the alias from std::experimental::filesystem to std::filesystem
// namespace std {
//    namespace filesystem = experimental::filesystem;
//}
//
//// We have a decent compiler and can use the normal version
//#   else
//// Include it
//#       include <filesystem>
//#   endif

#endif  // __cplusplus C++17
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#define INCLUDE_STD_FILESYSTEM 0
#define USE_FILESYSTEM 0
#endif  // #ifndef INCLUDE_STD_FILESYSTEM_EXPERIMENTAL
