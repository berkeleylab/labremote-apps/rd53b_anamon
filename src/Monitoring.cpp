// anamon
#include "Monitoring.h"

#include "FileUtils.h"
#include "RD53BAnalogMonitor.h"
//#include "EquipmentHelpers.h"

// labremote
#include "DataSinkConf.h"
#include "IDataSink.h"
#include "Logger.h"
#include "PowerSupplyChannel.h"

// test ps
#include "EquipConf.h"
#include "IPowerSupply.h"
#include "PowerSupplyChannel.h"

// json
#include <nlohmann/json.hpp>

// std/stl
#include <signal.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <thread>
#include <vector>

namespace anamon {

// have a global data sink object so that we can have a signal to clean it up...
// meh
std::shared_ptr<IDataSink> gstream = nullptr;
void close(int signal) {
    if (gstream) {
        gstream->endMeasurement();
    }
    exit(signal);
}

void run(RD53BAnalogMonitor& mon, std::string config_file, float set_temp,
         bool ignore_ps_measurements) {
    //
    // catch interrupts so that we may end the measurement gracefully
    //
    signal(SIGINT, anamon::close);
    signal(SIGTSTP, anamon::close);

    //
    // load the configuration
    //
    std::ifstream ifs(config_file);
    nlohmann::json j_config = nlohmann::json::parse(ifs);
    auto j_monitor_config = j_config.at("monitor_config");
    logger(logDEBUG) << "Loaded monitoring config:";
    logger(logDEBUG) << std::setw(4) << j_config;

    //
    // valid set temp?
    //
    bool add_temp_field = false;
    if (!(set_temp < -273.15)) {
        add_temp_field = true;
    }

    //
    // output storage information
    //
    std::string output_data_directory = "";
    std::string output_suffix = "";
    auto j_output_config = j_monitor_config.at("output");
    for (auto j : j_output_config.items()) {
        //
        // output path for any data
        //
        if (j.key() == "directory") {
            std::string tmp_dir = j.value().get<std::string>();
            if (!tmp_dir.empty() && !anamon::utils::path_exists(tmp_dir)) {
                logger(logWARNING) << "Output directory \"" << tmp_dir
                                   << "\" does not exist, creating it...";
                std::stringstream cmd;
                cmd << "mkdir -p " << tmp_dir;
                std::system(cmd.str().c_str());
                if (!anamon::utils::path_exists(tmp_dir)) {
                    std::stringstream err;
                    err << "Failed to create output directory \"" << tmp_dir
                        << "\"";
                    throw std::runtime_error(err.str());
                }
            }
            if (!tmp_dir.empty()) output_data_directory = tmp_dir;
        } else if (j.key() == "suffix") {
            //
            // a suffix to append to any data files
            //
            output_suffix = j.value().get<std::string>();
        }
    }
    // if not specified, create a default data directory
    if (output_data_directory.empty()) {
        output_data_directory =
            anamon::utils::create_default_data_dir(output_suffix);
    }
    logger(logINFO) << "Setting monitoring output directory: "
                    << output_data_directory;

    //
    // datastreams
    //
    auto j_datasinks_tmp = j_monitor_config.at("datasinks");
    if (j_datasinks_tmp.find("CSV") != j_datasinks_tmp.end()) {
        j_datasinks_tmp["CSV"]["directory"] = output_data_directory;
    }
    nlohmann::json j_datasinks;
    j_datasinks["datasinks"] = j_datasinks_tmp;
    j_datasinks["datastreams"] = j_monitor_config.at("datastreams");

    DataSinkConf ds;
    ds.setHardwareConfig(j_datasinks);
    if (gstream) gstream.reset();  // shouldn't happen
    gstream = ds.getDataStream("rd53b_anamon");

    //
    // setup measurement
    //
    auto j_measurement = j_monitor_config.at("measurement");
    int frequency_ms = j_measurement.at("frequency").get<int>();
    int n_measurements_to_record = j_measurement.at("count").get<int>();
    int time_ms = j_measurement.at("time").get<int>();

    // monitor PS information
    std::vector<std::shared_ptr<PowerSupplyChannel>> ps_channels;
    std::vector<std::string> ps_names;
    bool monitor_ps = false;
    if (!ignore_ps_measurements &&
        j_measurement.find("powersupply") != j_measurement.end()) {
        bool ok = true;
        auto j_ps = j_measurement.at("powersupply");

        EquipConf hw;
        logger(logINFO) << "Initializing PS hardware for monitoring";
        hw.setHardwareConfig(j_ps);

        if (j_ps.find("channels") == j_ps.end()) {
            logger(logWARNING)
                << "Cannot monitor power-supply, no \"channels\" field "
                   "specified in provided configuration";
            ps_channels.clear();
            ps_names.clear();
            ok = false;
        }
        if (ok) {
            for (const auto& ch : j_ps.at("channels").items()) {
                logger(logDEBUG)
                    << "Initializing PS channel \"" << ch.key() << "\"...";
                auto ps_chan = hw.getPowerSupplyChannel(ch.key());
                ps_channels.push_back(ps_chan);
                auto ps_cfg = ch.value();
                if (ps_cfg.find("simple-name") != ps_cfg.end()) {
                    ps_names.push_back(ps_cfg.at("simple-name"));
                } else {
                    ps_names.push_back(ch.key());
                }
            }  // loop over channels
        }

        if (ps_channels.size() > 0 && (ps_channels.size() == ps_names.size())) {
            if (ok)
                monitor_ps = true;
            else
                monitor_ps = false;
        }
    }  // powersupply

    // if empty, measure everything
    std::vector<std::string> quantities_to_measure =
        j_measurement.at("quantities").get<std::vector<std::string>>();
    if (quantities_to_measure.size() == 0) {
        quantities_to_measure = mon.get_list_of_quantities();
    }

    auto measurement_tag = j_measurement.at("tag").get<std::string>();
    auto measurement_name = j_measurement.at("name").get<std::string>();

    if (output_suffix != "") {
        measurement_tag += "_" + output_suffix;
        measurement_name += "_" + output_suffix;
    }

    gstream->setTag("measurement", measurement_tag);

    if (add_temp_field) {
        gstream->setTag("EnvTemp", set_temp);
    } else {
        gstream->setTag("EnvTemp", std::string("NaN"));
    }

    auto t_measurement_start = std::chrono::steady_clock::now();

    int n_measurements_recorded = 0;
    while (true) {
        auto t0 = std::chrono::steady_clock::now() +
                  std::chrono::milliseconds(frequency_ms);
        std::this_thread::sleep_until(t0);

        //
        // point time
        //
        auto tp = std::chrono::duration_cast<std::chrono::microseconds>(
                      std::chrono::system_clock::now().time_since_epoch())
                      .count();

        //
        // power-supply
        //
        if (monitor_ps) {
            for (size_t ps_idx = 0; ps_idx < ps_channels.size(); ps_idx++) {
                std::stringstream ps_meas;
                ps_meas << "PS" << ps_names.at(ps_idx);

                std::string ps_name = ps_names.at(ps_idx);
                double voltage = ps_channels.at(ps_idx)->measureVoltage();
                double voltage_set = ps_channels.at(ps_idx)->getVoltageLevel();
                double current = ps_channels.at(ps_idx)->measureCurrent();

                std::vector<double> vals{voltage_set, voltage, current};
                std::vector<std::string> var_names{ps_meas.str() + "_VSET",
                                                   ps_meas.str() + "_V",
                                                   ps_meas.str() + "_I"};

                for (size_t i = 0; i < vals.size(); i++) {
                    gstream->setField(var_names.at(i), vals.at(i));
                }  // i
            }      // ps_idx
        }          // monitor_ps

        auto measurements = mon.read(quantities_to_measure);
        if (measurements.size() == 0) {
            continue;
        }
        gstream->startMeasurement(measurement_name,
                                  std::chrono::system_clock::now());
        for (auto meas_map : measurements) {
            std::string field = meas_map.first;
            double value = meas_map.second;
            if (field.find("NTC") != std::string::npos) {
                value = mon.ntc_to_temp(value, false);
            }
            gstream->setField(field, value);
        }

        gstream->recordPoint();
        n_measurements_recorded++;

        gstream->endMeasurement();

        // if we are recording only a specific number of points
        if (n_measurements_to_record > 0 &&
            (n_measurements_recorded >= n_measurements_to_record))
            break;
        if (time_ms > 0) {
            auto t_now = std::chrono::steady_clock::now();
            std::chrono::duration<float, std::milli> elapsed =
                t_now - t_measurement_start;
            if (elapsed.count() > time_ms) break;
        }
    }
}

};  // namespace anamon
