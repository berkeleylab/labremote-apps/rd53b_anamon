// labremote
#include "RD53BAnalogMonitor.h"

#include "AD799X.h"
#include "FT232H.h"
#include "I2CFTDICom.h"
#include "Logger.h"

// json
using json = nlohmann::json;

extern "C" {
#include "mpsse.h"
}

// std/stl
#include <math.h>

#include <algorithm>  // sort
#include <chrono>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <map>
#include <sstream>
#include <thread>
#include <tuple>

RD53BAnalogMonitor::RD53BAnalogMonitor() {}

bool RD53BAnalogMonitor::init(std::string config_file) {
    //
    // load the configuration
    //
    std::ifstream ifs(config_file);
    json j_scc_config = json::parse(ifs);
    j_scc_config = j_scc_config.at("rd53b_scc_analog_monitor");

    //
    // load NTC constants
    //
    try {
        m_steinhart_coeffs.clear();
        auto j_ntc = j_scc_config.at("ntc");
        std::string ntc_part = j_ntc.at("ntc_part").get<std::string>();
        std::vector<float> steinhart_coeffs =
            j_ntc.at("steinhart_coeffs").get<std::vector<float>>();
        m_ntc_r_ref = j_ntc.at("r_ref").get<float>();

        logger(logDEBUG) << "Loading NTC coefficients for NTC part \""
                         << ntc_part << "\" (R_ref = " << m_ntc_r_ref
                         << " Ohms):";
        std::vector<std::string> c_names{"A", "B", "C"};
        for (size_t i = 0; i < steinhart_coeffs.size(); i++) {
            logger(logDEBUG) << "  Steinhart " << c_names.at(i) << ": "
                             << steinhart_coeffs.at(i);
            m_steinhart_coeffs.push_back(steinhart_coeffs.at(i));
        }
    } catch (std::exception& e) {
        logger(logERROR) << "Failed to load NTC constants, exception caught: "
                         << e.what();
        return false;
    }

    //
    // load ADC constants & calibration
    //
    try {
        m_adc_vref = -1;
        auto j_adc = j_scc_config.at("adc");
        m_adc_vref = j_adc.at("vref").get<float>();
        if (j_adc.find("i2c") != j_adc.end()) {
            auto j_i2c = j_adc.at("i2c");
            if (j_i2c.find("address-0") != j_i2c.end()) {
                m_adc_i2c_address_0 = j_i2c.at("address-0").get<uint32_t>();
            }
            if (j_i2c.find("address-1") != j_i2c.end()) {
                m_adc_i2c_address_1 = j_i2c.at("address-1").get<uint32_t>();
            }
        }
    } catch (std::exception& e) {
        logger(logERROR) << "Failed to load ADC constants, exception caught: "
                         << e.what();
        return false;
    }

    //
    // setup FTDI communication
    //
    try {
        std::string serial = "";
        std::string product = "";
        if (j_scc_config.find("connection") != j_scc_config.end()) {
            auto j_connection = j_scc_config.at("connection");
            if (j_connection.find("serial_number") != j_connection.end()) {
                serial = j_connection.at("serial_number");
                product = "SCCAnalogMonitor";
            }
        }
        logger(logDEBUG) << "Initializing communication with FT232H";
        logger(logDEBUG1) << "   Assumed device:";
        logger(logDEBUG1) << "       PRODUCT ID    : " << product;
        logger(logDEBUG1) << "       SERIAL NUMBER : " << serial;
        m_ft232 = std::make_shared<FT232H>(
            MPSSEChip::Protocol::I2C, MPSSEChip::Speed::FOUR_HUNDRED_KHZ,
            MPSSEChip::Endianness::MSBFirst, product, serial);
    } catch (std::exception& e) {
        logger(logERROR)
            << "Unable to initialize FT232H device, exception caught: "
            << e.what();
        logger(logERROR)
            << "Did you specify the correct FT232H device identity in the "
               "\"connection\" field of the SCC configuration file? Run "
               "\"./bin/ftdi_list\" to confirm.";
        return false;
    }

    //
    // setup ADCs
    //
    try {
        logger(logDEBUG) << "Initializing ADC #0 with I2C address 0x"
                         << std::hex << (unsigned)(m_adc_i2c_address_0);
        std::shared_ptr<I2CFTDICom> com0(
            new I2CFTDICom(m_ft232, m_adc_i2c_address_0));
        m_adc0 =
            std::make_shared<AD799X>(m_adc_vref, AD799X::Model::AD7998, com0);

        logger(logDEBUG) << "Initializing ADC #1 with I2C address 0x"
                         << std::hex << (unsigned)(m_adc_i2c_address_1);
        std::shared_ptr<I2CFTDICom> com1(
            new I2CFTDICom(m_ft232, m_adc_i2c_address_1));
        m_adc1 =
            std::make_shared<AD799X>(m_adc_vref, AD799X::Model::AD7998, com1);
    } catch (std::exception& e) {
        logger(logERROR) << "Unable to initialize ADCs, exception caught: "
                         << e.what();
        return false;
    }

    m_channel_map = {// ADC U2
                     {"VDDD", std::make_pair(0, 0)},
                     {"VREFA", std::make_pair(0, 1)},
                     {"VDDA", std::make_pair(0, 2)},
                     {"REXTA", std::make_pair(0, 3)},
                     {"VIND", std::make_pair(0, 4)},
                     {"REXTD", std::make_pair(0, 5)},
                     {"VINA", std::make_pair(0, 6)},
                     {"VREFD", std::make_pair(0, 7)},
                     // ADC U3
                     {"VREF_ADC", std::make_pair(1, 0)},
                     {"VMUX_OUT", std::make_pair(1, 1)},
                     {"VOFS_LP", std::make_pair(1, 2)},
                     {"R_IREF", std::make_pair(1, 3)},
                     {"VOFS", std::make_pair(1, 4)},
                     {"VDD_PRE", std::make_pair(1, 5)},
                     {"TP_NTC1", std::make_pair(1, 6)},
                     {"VREF_OVP", std::make_pair(1, 7)}};

    return true;
}

RD53BAnalogMonitor::~RD53BAnalogMonitor() {}

void RD53BAnalogMonitor::start_conversion() {
    // strobe the CONV pin
    m_ft232->gpio_write(PIN_CONV, 0);
    std::this_thread::sleep_for(std::chrono::microseconds(10));
    m_ft232->gpio_write(PIN_CONV, 1);
    std::this_thread::sleep_for(std::chrono::microseconds(10));
    m_ft232->gpio_write(PIN_CONV, 0);
}

std::vector<std::string> RD53BAnalogMonitor::get_list_of_quantities() {
    size_t n_q = m_channel_map.size();
    std::vector<std::string> measured_quantities;
    for (size_t adc_idx = 0; adc_idx < 2; adc_idx++) {
        for (size_t chan_idx = 0; chan_idx < 8; chan_idx++) {
            for (auto ch : m_channel_map) {
                std::string name = ch.first;
                uint32_t adc = std::get<0>(ch.second);
                uint32_t adc_channel = std::get<1>(ch.second);
                if (adc != adc_idx) continue;
                if (adc_channel != chan_idx) continue;
                measured_quantities.push_back(name);
            }
        }
    }
    return measured_quantities;
}

bool RD53BAnalogMonitor::valid_channel(std::string name) {
    auto find = m_channel_map.find(name);
    if (find == m_channel_map.end()) {
        return false;
    }
    return true;
}

uint32_t RD53BAnalogMonitor::name_to_channel(std::string name) {
    if (!valid_channel(name)) {
        std::stringstream e;
        e << "Invalid analog channel name \"" << name << "\"";
        throw std::runtime_error(e.str());
    }
    return std::get<1>(m_channel_map.at(name));
}

int32_t RD53BAnalogMonitor::readCount(std::string name) {
    if (!valid_channel(name)) {
        std::stringstream e;
        e << "Invalid analog channel name \"" << name << "\"";
        throw std::runtime_error(e.str());
    }

    auto adc_pair = m_channel_map.at(name);
    uint32_t adc_idx = std::get<0>(adc_pair);
    uint32_t channel = std::get<1>(adc_pair);
    if (adc_idx == 0) {
        return m_adc0->readCount(channel);
    } else {
        return m_adc1->readCount(channel);
    }
    return -1;
}

double RD53BAnalogMonitor::read(std::string name) {
    auto adc_pair = m_channel_map.at(name);
    uint32_t adc_idx = std::get<0>(adc_pair);
    uint32_t channel = std::get<1>(adc_pair);
    int32_t counts = -1;
    if (adc_idx == 0) {
        return m_adc0->read(channel);
    } else {
        return m_adc1->read(channel);
    }
    return -1;
}

void RD53BAnalogMonitor::get_adc_channels_and_fields(
    int select_adc_idx, const std::vector<std::string> input_fields,
    std::vector<uint8_t>& adc_channels, std::vector<std::string>& fields) {
    adc_channels.clear();
    fields.clear();

    auto channel_map = get_channel_map();
    for (const auto f : input_fields) {
        try {
            auto ch_info = channel_map.at(f);
            uint32_t adc_idx = std::get<0>(ch_info);
            uint32_t adc_channel = std::get<1>(ch_info);
            if (adc_idx == select_adc_idx) {
                adc_channels.push_back(adc_channel);
            }
        }  // try
        catch (std::exception& e) {
            logger(logWARNING)
                << "Quantity \"" << f << "\" is invalid, will not measure it";
            continue;
        }
    }  // f

    std::sort(adc_channels.begin(), adc_channels.end());
    fields.clear();

    for (const auto selected_channel : adc_channels) {
        for (const auto m : channel_map) {
            std::string field_name = m.first;
            auto ch_info = m.second;
            uint32_t adc_idx = std::get<0>(ch_info);
            uint32_t adc_channel = std::get<1>(ch_info);
            if (adc_channel == selected_channel && adc_idx == select_adc_idx) {
                fields.push_back(field_name);
                break;
            }
        }  // m
    }      // selected_channel

    return;
}

std::map<std::string, double> RD53BAnalogMonitor::read(
    const std::vector<std::string> input_fields) {
    // get those fields from \"input_fields\" that are associated with ADC 0
    std::vector<uint8_t> selected_channels_adc0;
    std::vector<std::string> fields_adc0;
    get_adc_channels_and_fields(0, input_fields, selected_channels_adc0,
                                fields_adc0);

    // get those fields from \"input_fields\" that are associated with ADC 1
    std::vector<uint8_t> selected_channels_adc1;
    std::vector<std::string> fields_adc1;
    get_adc_channels_and_fields(1, input_fields, selected_channels_adc1,
                                fields_adc1);

    std::vector<double> measurements_adc0(selected_channels_adc0.size());
    std::vector<double> measurements_adc1(selected_channels_adc1.size());

    // perform ADC sampling

    m_adc0->read(selected_channels_adc0, measurements_adc0);
    m_adc1->read(selected_channels_adc1, measurements_adc1);

    // build the output map, mapping input_field names to measurement
    std::map<std::string, double> output;
    for (size_t i = 0; i < measurements_adc0.size(); i++) {
        output[fields_adc0.at(i)] = measurements_adc0.at(i);
    }
    for (size_t i = 0; i < measurements_adc1.size(); i++) {
        output[fields_adc1.at(i)] = measurements_adc1.at(i);
    }
    return output;
}
std::map<std::string, int32_t> RD53BAnalogMonitor::readCount(
    const std::vector<std::string> input_fields) {
    // get those fields from \"input_fields\" that are associated with ADC 0
    std::vector<uint8_t> selected_channels_adc0;
    std::vector<std::string> fields_adc0;
    get_adc_channels_and_fields(0, input_fields, selected_channels_adc0,
                                fields_adc0);

    // get those fields from \"input_fields\" that are associated with ADC 1
    std::vector<uint8_t> selected_channels_adc1;
    std::vector<std::string> fields_adc1;
    get_adc_channels_and_fields(1, input_fields, selected_channels_adc1,
                                fields_adc1);

    std::vector<int32_t> measurements_adc0(selected_channels_adc0.size());
    std::vector<int32_t> measurements_adc1(selected_channels_adc1.size());

    // perform ADC sampling
    m_adc0->readCount(selected_channels_adc0, measurements_adc0);
    m_adc1->readCount(selected_channels_adc1, measurements_adc1);

    // build the output map, mapping input_field names to measurement
    std::map<std::string, int32_t> output;
    for (size_t i = 0; i < measurements_adc0.size(); i++) {
        output[fields_adc0.at(i)] = measurements_adc0.at(i);
    }
    for (size_t i = 0; i < measurements_adc1.size(); i++) {
        output[fields_adc1.at(i)] = measurements_adc1.at(i);
    }
    return output;
}

float RD53BAnalogMonitor::r_ntc_from_counts(int32_t counts) {
    float r_ntc = m_ntc_r_ref * (ADC_MAX_COUNTS / counts - 1);
    return r_ntc;
}

float RD53BAnalogMonitor::r_ntc_from_value(double value) {
    float r_ntc = m_ntc_r_ref * (m_adc_vref / value - 1);
    return r_ntc;
}

float RD53BAnalogMonitor::ntc_counts_to_temp(int32_t counts, bool in_kelvin) {
    float r_ntc = r_ntc_from_counts(counts);
    return get_ntc_temp_steinhart(r_ntc, in_kelvin);
}

float RD53BAnalogMonitor::ntc_to_temp(double value, bool in_kelvin) {
    float r_ntc = r_ntc_from_value(value);
    return get_ntc_temp_steinhart(r_ntc, in_kelvin);
}

float RD53BAnalogMonitor::get_ntc_temp(float r_ntc, bool in_kelvin) {
    float numerator = NTC_T_REF * NTC_BETA;
    float denominator = NTC_BETA + NTC_T_REF * log(r_ntc / NTC_R_REF);
    float tK = numerator / denominator;
    if (in_kelvin) return tK;
    return tK - 273.15;
}

float RD53BAnalogMonitor::get_ntc_temp_steinhart(float r_ntc, bool in_kelvin) {
    if (m_steinhart_coeffs.size() != 3) {
        throw std::runtime_error("Steinhart coefficients not loaded properly!");
    }

    float logres = log(r_ntc);
    float a = m_steinhart_coeffs.at(0);
    float b = m_steinhart_coeffs.at(1);
    float c = m_steinhart_coeffs.at(2);
    float tK = 1.0 / (a + b * logres + c * pow(logres, 3));
    if (in_kelvin) return tK;
    return tK - 273.15;
}
