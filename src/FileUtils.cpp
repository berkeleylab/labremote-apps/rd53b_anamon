#include "FileUtils.h"

// std/stl
#include <ctime>
#include <iomanip>  // put_time
#include <sstream>

#include "filesystem.h"
#if INCLUDE_STD_FILESYSTEM_EXPERIMENTAL && USE_FILESYSTEM
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#elif INCLUDE_STD_FILESYSTEM && USE_FILESYSTEM
#include <filesystem>
namespace fs = std::filesystem;
#elif !USE_FILESYSTEM
#include <sys/stat.h>
#include <unistd.h>  // getcwd
#endif

// labRemote
#include "Logger.h"

namespace anamon {
namespace utils {

std::string anamon_dir() {
#if USE_FILESYSTEM
    std::string cwd = fs::current_path();
    fs::path p = cwd;
    while (true) {
        std::string tmp = p.parent_path().filename();
        if (tmp == "rd53b_anamon") {
            return p.parent_path();
        }
        p = p.parent_path();
    }  // while
    return "";
#else
    char temp[1024];
    std::string cwd =
        getcwd(temp, sizeof(temp)) ? std::string(temp) : std::string("");

    std::stringstream base{cwd};
    std::stringstream anamon_dir;
    std::string tmp;
    while (std::getline(base, tmp, '/')) {
        anamon_dir << tmp << "/";
        if (tmp == "rd53b_anamon") break;
    }  // while
    return anamon_dir.str();
#endif
}

std::string dir_date_name(std::string base_dir_name, std::string suffix) {
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::stringstream date;
    date << std::put_time(&tm, "%Y-%m-%d");

#if USE_FILESYSTEM
    fs::path base = base_dir_name;
#else
    std::string base = base_dir_name;
#endif

    std::stringstream tmp_new_dir;
    for (unsigned i = 0; i < 9999; i++) {
        tmp_new_dir.str("");
        tmp_new_dir << date.str();
        if (suffix != "") {
            tmp_new_dir << "_" << suffix;
        }
        tmp_new_dir << "_" << std::setfill('0') << std::setw(4) << i;
#if USE_FILESYSTEM
        fs::path check_output_dir = base / tmp_new_dir.str();
        bool exists = anamon::utils::path_exists(check_output_dir);
#else
        std::stringstream check_output_dir;
        check_output_dir << base << "/" << tmp_new_dir.str();
        bool exists =
            anamon::utils::path_exists(check_output_dir.str().c_str());
#endif
        if (!exists) {
            return std::string(check_output_dir.str());
        }
    }  // i

    // error case, return empty string
    return "";
}

std::string create_default_data_dir(std::string suffix) {
    std::string am_dir = anamon_dir();
#if USE_FILESYSTEM
    if (am_dir == "") return fs::current_path();
    fs::path p_data(am_dir);
    p_data /= "data";
    std::string date_dir = anamon::utils::dir_date_name(p_data, suffix);
    fs::create_directories(date_dir);
#else
    char temp[1024];
    std::string cwd =
        getcwd(temp, sizeof(temp)) ? std::string(temp) : std::string("");
    if (am_dir == "") return cwd;
    std::stringstream p_data;
    p_data << am_dir;
    p_data << "/data";
    std::string date_dir = anamon::utils::dir_date_name(p_data.str(), suffix);

    std::stringstream cmd;
    cmd << "mkdir -p " << date_dir;
    std::system(cmd.str().c_str());
#endif
    anamon::utils::create_dir_symlink(date_dir, "last_output");
    return date_dir;
}

std::string replace_substr(std::string in, std::string old_sub,
                           std::string new_sub) {
    std::string out = in;
    while (out.find(old_sub) != std::string::npos) {
        size_t idx = out.find(old_sub);
        out.replace(out.begin() + idx, out.begin() + idx + old_sub.length(),
                    new_sub);
    }
    return out;
}

bool create_dir_symlink(std::string existing_dir, std::string sym_link_name) {
#if USE_FILESYSTEM
    fs::path existing = existing_dir;
    fs::path parent = existing.parent_path();
    fs::path sym = parent / sym_link_name;
    if (anamon::utils::path_exists(sym)) fs::remove(sym);
    std::stringstream sym_str;
    sym_str << "ln -s " << existing_dir << " " << std::string(sym);
    std::system(sym_str.str().c_str());
    bool exists = anamon::utils::path_exists(sym);
#else

    // remove any trailing forward slashes
    if (existing_dir.back() == '/') {
        existing_dir = existing_dir.substr(0, existing_dir.length() - 1);
    }
    std::string existing = existing_dir;
    std::size_t pos = existing.find_last_of('/');
    std::string name_of_last_part = existing.substr(pos + 1, existing.length());
    std::string sym_str = anamon::utils::replace_substr(
        existing, name_of_last_part, "last_output");

    bool sym_already_exists = anamon::utils::path_exists(sym_str);
    if (sym_already_exists) {
        std::stringstream rm;
        rm << "rm " << sym_str;
        std::system(rm.str().c_str());
    }

    std::stringstream cmd;
    cmd << "ln -s " << existing_dir << " " << sym_str;
    std::system(cmd.str().c_str());
    struct stat st;
    bool exists = anamon::utils::path_exists(sym_str);
#endif
    if (!exists) {
        logger(logWARNING) << "Failed to create symbolic link to directory (="
                           << existing_dir << ")";
        return false;
    }
    return true;
}

bool path_exists(std::string name) {
#if USE_FILESYSTEM
    return fs::exists(fs::path(name));
#else
    struct stat st;
    return (stat(name.c_str(), &st) == 0);
#endif
}

};  // namespace utils
};  // namespace anamon
